package com.mikhail.pokedex.misc;

import android.support.v4.app.*;
import android.support.v4.view.*;
import android.util.*;
import android.view.*;
import com.mikhail.pokedex.fragments.*;

public abstract class InfoFragmentPagerAdapter<T> extends PagerAdapter{

	FragmentManager fragmentManager;
	InfoPagerFragment<T>[] fragments;

	public abstract int getNumFrags();
	public abstract InfoPagerFragment<T> getFragment(int position);

	

	public InfoFragmentPagerAdapter(FragmentManager fm){
		fragmentManager = fm;
		fragments = new InfoPagerFragment[getNumFrags()];

	}

	public void resetFragments(){
		fragments = new InfoPagerFragment[getNumFrags()];
		notifyDataSetChanged();
	}
	
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object){
		assert(0 <= position && position < fragments.length);
		FragmentTransaction trans = fragmentManager.beginTransaction();
		trans.remove(fragments[position]);
		trans.commit();
		fragments[position] = null;
	}
	
	public void destroy(){
		int len = getCount();
		for(int i=0;i<len;i++){
			destroyItem(null, i, null);
		}
	}

	@Override
	public Fragment instantiateItem(ViewGroup container, int position){
		Fragment fragment = getItem(position);
		FragmentTransaction trans = fragmentManager.beginTransaction();
		Fragment old = fragmentManager.findFragmentByTag("fragment:"+position);
		if(old != null){
			trans.remove(old);
		}
		trans.add(container.getId(), fragment, "fragment:" + position);
		trans.commit();
		return fragment;
	}

	@Override
	public int getCount(){
		return fragments.length;
	}

	@Override
	public boolean isViewFromObject(View view, Object fragment){
		return ((Fragment) fragment).getView() == view;
	}

	public InfoPagerFragment<T> getItem(int position){
		assert(0 <= position && position < fragments.length);
		if (fragments[position] == null){
			fragments[position] = getFragment(position);
		}
		return fragments[position];
	}

	@Override
	public CharSequence getPageTitle(int position){
		return getItem(position).getTitle();
	}

	public void setData(T data){
		for (int f=0;f < getCount();f++){
			getItem(f).loadData(data);
		}
	}


	public void onPageSelected(int p1){

		for (int i=0;i < fragments.length;i++){
            if(fragments[i] != null) {
                fragments[i].setPagePrimary(i == p1);
            }
		}

	}

}
