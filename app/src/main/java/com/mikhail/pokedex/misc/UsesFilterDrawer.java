package com.mikhail.pokedex.misc;

import android.view.*;

public interface UsesFilterDrawer
{
	
	public View getRightDrawerLayout(LayoutInflater inflater, ViewGroup container);
	public void clearFilters();
}
